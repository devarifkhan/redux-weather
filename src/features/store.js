import { configureStore } from "@reduxjs/toolkit";
import forecastSlice from "./slice/forecastSlice";
import userSlice from "./slice/userSlice";
// import futureSlice from "./slice/futureSlice";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({
  forecast: forecastSlice,
  user: userSlice,
  // future: futureSlice,
});

export const store = configureStore({
  reducer: rootReducer,
});
