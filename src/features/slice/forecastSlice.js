import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchForecast = createAsyncThunk("fetchForecast", async (city) => {
  const response = await fetch(
    `https://api.weatherapi.com/v1/forecast.json?key=7b72a2f9ebec4cd988824901241202&q=${city}&days=6`
  );
  const data = await response.json();
  return data;
});

const forecastSlice = createSlice({
  name: "forecast",
  initialState: {
    loading: false,
    forecast: null,
    error: null,
    available: false,
  },
  extraReducers: (builder) => {
    builder.addCase(fetchForecast.pending, (state) => {
      state.loading = true;
      state.forecast = "loading";
      state.error = "";
    });

    builder.addCase(fetchForecast.fulfilled, (state, action) => {
      state.forecast = action.payload;
      state.loading = false;
      state.available = true;
    });
    builder.addCase(fetchForecast.rejected, (state, action) => {
      state.error = action.error.message;
    });
  },
});

export default forecastSlice.reducer;
