import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchFuturecast = createAsyncThunk(
  "fetchForecast",
  async (city) => {
    const response = await fetch(
      `http://api.weatherapi.com/v1/forecast.json?key=7b72a2f9ebec4cd988824901241202&q=${city}&days=6`
    );
    const data = await response.json();
    return data;
  }
);

const futureSlice = createSlice({
  name: "future",
  initialState: {
    loading: false,
    future: null,
    error: null,
    available: false,
  },
  extraReducers: (builder) => {
    builder.addCase(fetchFuturecast.pending, (state) => {
      state.loading = true;
      state.future = "loading";
      state.error = "";
    });

    builder.addCase(fetchFuturecast.fulfilled, (state, action) => {
      state.future = action.payload;
      state.loading = false;
      state.available = true;
    });
    builder.addCase(fetchFuturecast.rejected, (state, action) => {
      state.error = action.error.message;
    });
  },
});

export default futureSlice.reducer;
