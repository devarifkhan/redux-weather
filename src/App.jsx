import { useSelector, useDispatch } from "react-redux";
import "./App.css";
import SearchButton from "./components/SearchButton";
import WeatherCard from "./components/WeatherCard";
import { setUserName } from "./features/slice/userSlice";
// import { useState } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

function App() {
  const dispatch = useDispatch();
  const userName = useSelector((state) => state.user.userName);
  const foreCast = useSelector((state) => state.forecast.available);
  // const futureCast = useSelector((state) => state.future.available);
  // console.log(futureCast);
  // console.log(foreCast);

  // console.log(userName);

  // const [nameOfU, setNameOfU] = useState("");

  // const handleClick = () => {
  //   dispatch(setUserName(nameOfU));
  // };

  const userSchema = Yup.object().shape({
    name: Yup.string()

      .min(4, "Username must be at least 4 characters long")

      .max(20, "Username cannot be longer than 20 characters long")

      .required("A name is required"),
  });

  return (
    <>
      <h1 className="text-4xl mt-5 font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white text-center">
        Weather Redux Toolkit
      </h1>
      <div className="container mt-5 mx-auto">
        {userName === "" ? (
          <div className="w-60">
            <label
              htmlFor="first_name"
              className="block text-2xl mb-2  font-medium text-gray-900 dark:text-white"
            >
              Your Name
            </label>
            <div>
              <Formik
                initialValues={{
                  name: "",
                }}
                validationSchema={userSchema}
                onSubmit={(values) => {
                  // same shape as initial values
                  dispatch(setUserName(values.name));
                }}
              >
                {({ errors, touched }) => (
                  <Form>
                    <div className="flex">
                      <div>
                        <Field
                          name="name"
                          placeholder="Ariful Islam"
                          className="bg-gray-50 border border-gray-300 h-11 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-64 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        />

                        {errors.name && touched.name ? (
                          // <div
                          //   className="bg-green-100 border mt-2 border-green-400 text-black px-3 py-2 rounded relative"
                          //   role="alert"
                          // >
                          //   {errors.name}
                          // </div>

                          <div
                            className="rounded mt-2 bg-orange-100 border-l-4 w-full border-orange-500 text-orange-700 p-4"
                            role="alert"
                          >
                            {/* <p className="font-bold">Be Warned</p> */}
                            <p className="font-bold">{errors.name}.</p>
                          </div>
                        ) : null}
                      </div>
                      <button
                        type="submit"
                        className="ml-5 bg-purple-50 border h-11 border-purple-300 text-gray-900 text-sm rounded-lg focus:ring-purple-500 focus:purple-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      >
                        Done
                      </button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        ) : (
          <h2 className="text-3xl">
            Welcome,
            <span className="ms-2 text-3xl font-semibold text-gray-500 dark:text-gray-400">
              {userName}
            </span>
          </h2>
        )}

        <SearchButton />
        {foreCast === true ? <WeatherCard /> : ""}
      </div>
    </>
  );
}

export default App;
