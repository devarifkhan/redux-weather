import { fetchForecast } from ".././features/slice/forecastSlice";
// import { fetchFuturecast } from "../features/slice/futureSlice";
import { useDispatch } from "react-redux";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

export default function SearchButton() {
  const dispatch = useDispatch();

  const locationSchema = Yup.object().shape({
    location: Yup.string()

      .min(3, "Location must be at least 3 characters long")

      .max(20, "Username cannot be longer than 20 characters long")

      .required("Loaction can't be empty"),
  });

  return (
    <div className="max-w-sm">
      <h2 className="text-3xl font-extrabold dark:text-white">Location</h2>
      <div className="flex">
        <div className="relative">
          <Formik
            initialValues={{
              location: "",
            }}
            validationSchema={locationSchema}
            onSubmit={(values) => {
              // same shape as initial values
              dispatch(fetchForecast(values.location));
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <div className="flex">
                  <div>
                    <Field
                      name="location"
                      placeholder="Dhaka"
                      className="bg-gray-50 border border-gray-300 h-11 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-64 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    />

                    {errors.location && touched.location ? (
                      <div
                        className="rounded mt-2 bg-orange-100 border-l-4 w-full border-orange-500 text-orange-700 p-4"
                        role="alert"
                      >
                        {/* <p className="font-bold">Be Warned</p> */}
                        <p className="font-bold">{errors.location}.</p>
                      </div>
                    ) : null}
                  </div>
                  <button
                    type="submit"
                    className="ml-5 bg-green-50 border h-11 border-yellow-300 text-blue-900 text-sm rounded-lg focus:ring-purple-500 focus:purple-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    Search
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
