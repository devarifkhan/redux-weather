import { useSelector } from "react-redux";
import sunny from "../images/sunny.png";
import cloudy from "../images/cloudy.png";
import rainy from "../images/rainy.png";
import { useEffect, useState } from "react";

export default function WeatherCard() {
  const forecastIsAvailable = useSelector((state) => state.forecast.available);
  const forecast = useSelector((state) => state.forecast.forecast);
  // const futureIsAvailable = useSelector((state) => state.future.available);
  // const future = useSelector((state) => state.future.future);

  const [customDate_for_1, setCustomDate_for_1] = useState("");
  const [customDate_for_2, setCustomDate_for_2] = useState("");
  const [customDate_for_3, setCustomDate_for_3] = useState("");
  const [customDate_for_4, setCustomDate_for_4] = useState("");
  const [customDate_for_5, setCustomDate_for_5] = useState("");
  const [avgHumidity_for_1, setAvgHumidity_for_1] = useState(0);
  const [maxTemp_for_1, setMaxTemp_for_1] = useState(0);
  const [minTemp_for_1, setMinTemp_for_1] = useState(0);
  const [avgHumidity_for_2, setAvgHumidity_for_2] = useState(0);
  const [maxTemp_for_2, setMaxTemp_for_2] = useState(0);
  const [minTemp_for_2, setMinTemp_for_2] = useState(0);
  const [avgHumidity_for_3, setAvgHumidity_for_3] = useState(0);
  const [maxTemp_for_3, setMaxTemp_for_3] = useState(0);
  const [minTemp_for_3, setMinTemp_for_3] = useState(0);
  const [avgHumidity_for_4, setAvgHumidity_for_4] = useState(0);
  const [maxTemp_for_4, setMaxTemp_for_4] = useState(0);
  const [minTemp_for_4, setMinTemp_for_4] = useState(0);
  const [avgHumidity_for_5, setAvgHumidity_for_5] = useState(0);
  const [maxTemp_for_5, setMaxTemp_for_5] = useState(0);
  const [minTemp_for_5, setMinTemp_for_5] = useState(0);
  const [next_1_day_isSunny, setNext_1_day_isSunny] = useState(false);
  const [next_1_day_isColudy, setNext_1_day_isColudy] = useState(false);
  const [next_1_day_isRainy, setNext_1_day_isRainy] = useState(false);

  const [next_2_day_isSunny, setNext_2_day_isSunny] = useState(false);
  const [next_2_day_isColudy, setNext_2_day_isColudy] = useState(false);
  const [next_2_day_isRainy, setNext_2_day_isRainy] = useState(false);

  const [next_3_day_isSunny, setNext_3_day_isSunny] = useState(false);
  const [next_3_day_isColudy, setNext_3_day_isColudy] = useState(false);
  const [next_3_day_isRainy, setNext_3_day_isRainy] = useState(false);

  const [next_4_day_isSunny, setNext_4_day_isSunny] = useState(false);
  const [next_4_day_isColudy, setNext_4_day_isColudy] = useState(false);
  const [next_4_day_isRainy, setNext_4_day_isRainy] = useState(false);

  const [next_5_day_isSunny, setNext_5_day_isSunny] = useState(false);
  const [next_5_day_isColudy, setNext_5_day_isColudy] = useState(false);
  const [next_5_day_isRainy, setNext_5_day_isRainy] = useState(false);

  const [forecastHour, setForecastHour] = useState([]);
  const [temp_for_0_index, set_temp_for_0_index] = useState(0);
  const [humidity_for_0_index, set_humidity_for_0_index] = useState(0);
  const [dew_for_0_index, set_dew_for_0_index] = useState(0);
  const [is_sunny_for_0, set_is_sunny_for_0] = useState(false);
  const [is_coludy_for_0, set_is_coludy_for_0] = useState(false);
  const [is_rainy_for_0, set_is_rainy_for_0] = useState(false);
  const [time_for_0, set_time_for_0] = useState("");

  const [temp_for_1_index, set_temp_for_1_index] = useState(0);
  const [humidity_for_1_index, set_humidity_for_1_index] = useState(0);
  const [dew_for_1_index, set_dew_for_1_index] = useState(0);
  const [is_sunny_for_1, set_is_sunny_for_1] = useState(false);
  const [is_coludy_for_1, set_is_coludy_for_1] = useState(false);
  const [is_rainy_for_1, set_is_rainy_for_1] = useState(false);
  const [time_for_1, set_time_for_1] = useState("");

  const [temp_for_2_index, set_temp_for_2_index] = useState(0);
  const [humidity_for_2_index, set_humidity_for_2_index] = useState(0);
  const [dew_for_2_index, set_dew_for_2_index] = useState(0);
  const [is_sunny_for_2, set_is_sunny_for_2] = useState(false);
  const [is_coludy_for_2, set_is_coludy_for_2] = useState(false);
  const [is_rainy_for_2, set_is_rainy_for_2] = useState(false);
  const [time_for_2, set_time_for_2] = useState("");

  const [temp_for_3_index, set_temp_for_3_index] = useState(0);
  const [humidity_for_3_index, set_humidity_for_3_index] = useState(0);
  const [dew_for_3_index, set_dew_for_3_index] = useState(0);
  const [is_sunny_for_3, set_is_sunny_for_3] = useState(false);
  const [is_coludy_for_3, set_is_coludy_for_3] = useState(false);
  const [is_rainy_for_3, set_is_rainy_for_3] = useState(false);
  const [time_for_3, set_time_for_3] = useState("");

  const [temp_for_4_index, set_temp_for_4_index] = useState(0);
  const [humidity_for_4_index, set_humidity_for_4_index] = useState(0);
  const [dew_for_4_index, set_dew_for_4_index] = useState(0);
  const [is_sunny_for_4, set_is_sunny_for_4] = useState(false);
  const [is_coludy_for_4, set_is_coludy_for_4] = useState(false);
  const [is_rainy_for_4, set_is_rainy_for_4] = useState(false);
  const [time_for_4, set_time_for_4] = useState("");

  const [currentTemperature, setCurrentTemperature] = useState(0);
  const [currHumidity, setCurrHumidity] = useState(0);
  const [locationName, setLocationName] = useState("");
  const [countryName, setCountryName] = useState("");
  const [currentIsSunny, setCurrentIsSunny] = useState(false);
  const [currentIsColudy, setCurrentIsColudy] = useState(false);
  const [currentIsRainy, setCurrentIsRainy] = useState(false);
  const [futureCast, setFutureCast] = useState([]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setFutureCast(forecast.forecast.forecastday);
      setCurrentTemperature(forecast.current.temp_c);
      setCurrHumidity(forecast.current.humidity);
      setLocationName(forecast.location.name);
      setCountryName(forecast.location.country);
      setForecastHour(forecast.forecast.forecastday["0"].hour);

      //for upper
      setCurrentIsSunny(currentTemperature >= 16 && currHumidity <= 60);
      setCurrentIsColudy(currentTemperature <= 15 || currHumidity >= 35);
      setCurrentIsRainy(currentTemperature <= 15);

      //for temp, humidity, dewpoint 0
      set_temp_for_0_index(forecastHour[next_5_hour[0]].temp_c);
      set_humidity_for_0_index(forecastHour[next_5_hour[0]].humidity);
      set_dew_for_0_index(forecastHour[next_5_hour[0]].dewpoint_c);
      set_is_sunny_for_0(
        temp_for_0_index >= 20 &&
          humidity_for_0_index <= 60 &&
          dew_for_0_index <= temp_for_0_index - 12
      );
      set_is_coludy_for_0(
        temp_for_0_index <= 15 ||
          humidity_for_0_index >= 80 ||
          dew_for_0_index >= temp_for_0_index - 5
      );
      set_is_rainy_for_0(dew_for_0_index >= temp_for_0_index);

      set_time_for_0(forecastHour[next_5_hour[0]].time.slice(-6));

      //for temp, humidity, dewpoint 1
      set_temp_for_1_index(forecastHour[next_5_hour[1]].temp_c);
      set_humidity_for_1_index(forecastHour[next_5_hour[1]].humidity);
      set_dew_for_1_index(forecastHour[next_5_hour[1]].dewpoint_c);

      set_is_sunny_for_1(
        temp_for_1_index >= 20 &&
          humidity_for_1_index <= 60 &&
          dew_for_1_index <= temp_for_1_index - 12
      );

      set_is_coludy_for_1(
        temp_for_1_index <= 15 ||
          humidity_for_1_index >= 80 ||
          dew_for_1_index >= temp_for_1_index - 5
      );
      set_is_rainy_for_1(dew_for_1_index >= temp_for_1_index);

      set_time_for_1(forecastHour[next_5_hour[1]].time.slice(-6));

      //for temp, humidity, dewpoint 2
      set_temp_for_2_index(forecastHour[next_5_hour[2]].temp_c);
      set_humidity_for_2_index(forecastHour[next_5_hour[2]].humidity);
      set_dew_for_2_index(forecastHour[next_5_hour[2]].dewpoint_c);
      set_is_sunny_for_2(
        temp_for_2_index >= 20 &&
          humidity_for_2_index <= 60 &&
          dew_for_2_index <= temp_for_2_index - 12
      );
      set_is_coludy_for_2(
        temp_for_2_index <= 15 ||
          humidity_for_2_index >= 80 ||
          dew_for_2_index >= temp_for_2_index - 5
      );
      set_is_rainy_for_2(dew_for_2_index >= temp_for_2_index);
      set_time_for_2(forecastHour[next_5_hour[2]].time.slice(-6));

      //for temp, humidity, dewpoint 3
      set_temp_for_3_index(forecastHour[next_5_hour[3]].temp_c);
      set_humidity_for_3_index(forecastHour[next_5_hour[3]].humidity);
      set_dew_for_3_index(forecastHour[next_5_hour[3]].dewpoint_c);
      set_is_sunny_for_3(
        temp_for_3_index >= 20 &&
          humidity_for_3_index <= 60 &&
          dew_for_3_index <= temp_for_3_index - 12
      );
      set_is_coludy_for_3(
        temp_for_3_index <= 15 ||
          humidity_for_3_index >= 80 ||
          dew_for_3_index >= temp_for_3_index - 5
      );
      set_is_rainy_for_3(dew_for_3_index >= temp_for_3_index);
      set_time_for_3(forecastHour[next_5_hour[3]].time.slice(-6));

      //for temp, humidity, dewpoint 4
      set_temp_for_4_index(forecastHour[next_5_hour[4]].temp_c);
      set_humidity_for_4_index(forecastHour[next_5_hour[4]].humidity);
      set_dew_for_4_index(forecastHour[next_5_hour[4]].dewpoint_c);
      set_is_sunny_for_4(
        temp_for_4_index >= 20 &&
          humidity_for_4_index <= 60 &&
          dew_for_4_index <= temp_for_4_index - 12
      );
      set_is_coludy_for_4(
        temp_for_4_index <= 15 ||
          humidity_for_4_index >= 80 ||
          dew_for_4_index >= temp_for_4_index - 5
      );
      set_is_rainy_for_4(dew_for_4_index >= temp_for_4_index);
      set_time_for_4(forecastHour[next_5_hour[4]].time.slice(-6));

      setAvgHumidity_for_1(futureCast[1].day.avghumidity);
      setMaxTemp_for_1(futureCast[1].day.maxtemp_c);
      setMinTemp_for_1(futureCast[1].day.mintemp_c);

      setAvgHumidity_for_2(futureCast[2].day.avghumidity);
      setMaxTemp_for_2(futureCast[2].day.maxtemp_c);
      setMinTemp_for_2(futureCast[2].day.mintemp_c);

      setAvgHumidity_for_3(futureCast[3].day.avghumidity);
      setMaxTemp_for_3(futureCast[3].day.maxtemp_c);
      setMinTemp_for_3(futureCast[3].day.mintemp_c);

      setAvgHumidity_for_4(futureCast[4].day.avghumidity);
      setMaxTemp_for_4(futureCast[4].day.maxtemp_c);
      setMinTemp_for_4(futureCast[4].day.mintemp_c);

      setAvgHumidity_for_5(futureCast[5].day.avghumidity);
      setMaxTemp_for_5(futureCast[5].day.maxtemp_c);
      setMinTemp_for_5(futureCast[5].day.mintemp_c);

      setNext_1_day_isSunny(
        futureCast[1].day.maxtemp_c >= 16 && futureCast[1].day.avghumidity <= 60
      );
      setNext_1_day_isColudy(
        futureCast[1].day.maxtemp_c <= 15 || futureCast[1].day.avghumidity >= 35
      );
      setNext_1_day_isRainy(futureCast[1].day.maxtemp_c <= 15);

      setNext_2_day_isSunny(
        futureCast[2].day.maxtemp_c >= 16 && futureCast[2].day.avghumidity <= 60
      );
      setNext_2_day_isColudy(
        futureCast[2].day.maxtemp_c <= 15 || futureCast[2].day.avghumidity >= 35
      );
      setNext_2_day_isRainy(futureCast[2].day.maxtemp_c <= 15);

      setNext_3_day_isSunny(
        futureCast[3].day.maxtemp_c >= 16 && futureCast[3].day.avghumidity <= 60
      );
      setNext_3_day_isColudy(
        futureCast[3].day.maxtemp_c <= 15 || futureCast[3].day.avghumidity >= 35
      );
      setNext_3_day_isRainy(futureCast[3].day.maxtemp_c <= 15);

      setNext_4_day_isSunny(
        futureCast[4].day.maxtemp_c >= 16 && futureCast[4].day.avghumidity <= 60
      );
      setNext_4_day_isColudy(
        futureCast[4].day.maxtemp_c <= 15 || futureCast[4].day.avghumidity >= 35
      );
      setNext_4_day_isRainy(futureCast[4].day.maxtemp_c <= 15);

      setNext_5_day_isSunny(
        futureCast[5].day.maxtemp_c >= 16 && futureCast[5].day.avghumidity <= 60
      );
      setNext_5_day_isColudy(
        futureCast[5].day.maxtemp_c <= 15 || futureCast[5].day.avghumidity >= 35
      );
      setNext_5_day_isRainy(futureCast[5].day.maxtemp_c <= 15);

      const date_for_1 = futureCast["1"].date;
      const dateObject_for_1 = new Date(date_for_1);
      const dayNames = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
      ];
      const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      //day Name
      const dayName_for_1 = dayNames[dateObject_for_1.getDay()];

      //get date
      const date = dateObject_for_1.getDate();
      //get month

      const month_for_1 = months[dateObject_for_1.getMonth()];

      console.log(dayName_for_1.slice(0, 3));
      console.log(date);
      console.log(month_for_1.slice(0, 3));

      const customDate_for_1 = `${dayName_for_1.slice(
        0,
        3
      )}, ${date} ${month_for_1.slice(0, 3)}`;
      setCustomDate_for_1(customDate_for_1);

      const date_for_2 = futureCast["2"].date;
      const dayName_for_2 = dayNames[new Date(date_for_2).getDay()];
      const date_for2 = new Date(date_for_2).getDate();
      const month_for_2 = months[new Date(date_for_2).getMonth()];

      console.log(dayName_for_2, date_for2, month_for_2);
      const customDate_for_2 = `${dayName_for_2.slice(
        0,
        3
      )}, ${date_for2} ${month_for_2.slice(0, 3)}`;
      setCustomDate_for_2(customDate_for_2);

      // custom date for 3
      const date_for_3 = futureCast["3"].date;
      const dayName_for_3 = dayNames[new Date(date_for_3).getDay()];
      const date_for3 = new Date(date_for_3).getDate();
      const month_for_3 = months[new Date(date_for_3).getMonth()];

      console.log(dayName_for_3, date_for3, month_for_3);
      const customDate_for_3 = `${dayName_for_3.slice(
        0,
        3
      )}, ${date_for3} ${month_for_3.slice(0, 3)}`;
      setCustomDate_for_3(customDate_for_3);

      // custom date for 4
      const date_for_4 = futureCast["4"].date;
      const dayName_for_4 = dayNames[new Date(date_for_4).getDay()];
      const date_for4 = new Date(date_for_4).getDate();
      const month_for_4 = months[new Date(date_for_4).getMonth()];
      const customDate_for_4 = `${dayName_for_4.slice(
        0,
        3
      )}, ${date_for4} ${month_for_4.slice(0, 3)}`;
      setCustomDate_for_4(customDate_for_4);

      // custom date for 5
      const date_for_5 = futureCast["5"].date;
      const dayName_for_5 = dayNames[new Date(date_for_5).getDay()];
      const date_for5 = new Date(date_for_5).getDate();
      const month_for_5 = months[new Date(date_for_5).getMonth()];
      const customDate_for_5 = `${dayName_for_5.slice(
        0,
        3
      )}, ${date_for5} ${month_for_5.slice(0, 3)}`;
      setCustomDate_for_5(customDate_for_5);
    }, 2000);

    return () => clearTimeout(timer);
  });

  const currentDate = new Date();

  const hours = currentDate.getHours();
  const hours_5 = (hours + 1 + 5) % 24;
  let next_5_hour = [];

  for (let hour = hours + 1; hour < hours_5; hour++) {
    next_5_hour.push(hour);
  }

  function convertTo12HourFormat(time) {
    const [hours, minutes] = time.split(":");
    let period = "AM";

    let hoursIn12HrFormat = parseInt(hours);
    if (hoursIn12HrFormat >= 12) {
      hoursIn12HrFormat %= 12;
      period = "PM";
    }

    // Handle midnight (00:00) as 12:00 AM
    if (hoursIn12HrFormat === 0) {
      hoursIn12HrFormat = 12;
    }

    return `${hoursIn12HrFormat}:${minutes} ${period}`;
  }

  return (
    <div className="mt-5">
      <div className="flex flex-col items-center justify-center  text-gray-700 p-10 bg-gradient-to-br from-pink-200 rounded-lg via-purple-200 to-indigo-200">
        <div className="w-full max-w-screen-sm bg-white p-10 rounded-xl ring-8 ring-white ring-opacity-40">
          {forecastIsAvailable === true && temp_for_0_index !== "" ? (
            <div className="flex justify-between">
              <div className="flex flex-col">
                <span className="text-6xl font-bold">
                  {currentTemperature}°C
                </span>

                <span className="font-semibold mt-1 text-gray-500">
                  {locationName},{countryName}
                </span>
              </div>
              {currentIsSunny === true ? (
                <img src={sunny} className="h-auto w-20"></img>
              ) : (
                ""
              )}
              {currentIsColudy === true ? (
                <img src={cloudy} className="h-auto w-20"></img>
              ) : (
                ""
              )}

              {currentIsRainy === true ? (
                <img src={rainy} className="h-auto w-20"></img>
              ) : (
                ""
              )}
            </div>
          ) : (
            ""
          )}
          <div className="flex justify-between mt-12">
            <div className="flex flex-col items-center">
              <span className="font-semibold text-lg">
                {temp_for_0_index}°C
              </span>
              {is_sunny_for_0 === true ? (
                <img src={sunny} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_coludy_for_0 === true ? (
                <img src={cloudy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_rainy_for_0 === true ? (
                <img src={rainy} className="h-auto w-12"></img>
              ) : (
                ""
              )}

              <span className="font-semibold mt-1 text-sm">
                {convertTo12HourFormat(time_for_0)}
              </span>
              {/* <span className="text-xs font-semibold text-gray-400">AM</span> */}
            </div>
            <div className="flex flex-col items-center">
              <span className="font-semibold text-lg">
                {temp_for_1_index}°C
              </span>
              {is_sunny_for_1 === true ? (
                <img src={sunny} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_coludy_for_1 === true ? (
                <img src={cloudy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_rainy_for_1 === true ? (
                <img src={rainy} className="h-auto w-12"></img>
              ) : (
                ""
              )}

              <span className="font-semibold mt-1 text-sm">
                {convertTo12HourFormat(time_for_1)}
              </span>
              {/* <span className="text-xs font-semibold text-gray-400">PM</span> */}
            </div>
            <div className="flex flex-col items-center">
              <span className="font-semibold text-lg">
                {temp_for_2_index}°C
              </span>
              {is_sunny_for_2 === true ? (
                <img src={sunny} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_coludy_for_2 === true ? (
                <img src={cloudy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_rainy_for_2 === true ? (
                <img src={rainy} className="h-auto w-12"></img>
              ) : (
                ""
              )}

              <span className="font-semibold mt-1 text-sm">
                {convertTo12HourFormat(time_for_2)}
              </span>
              {/* <span className="text-xs font-semibold text-gray-400">PM</span> */}
            </div>
            <div className="flex flex-col items-center">
              <span className="font-semibold text-lg">
                {temp_for_3_index}°C
              </span>
              {is_sunny_for_3 === true ? (
                <img src={sunny} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_coludy_for_3 === true ? (
                <img src={cloudy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_rainy_for_3 === true ? (
                <img src={rainy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              <span className="font-semibold mt-1 text-sm">
                {convertTo12HourFormat(time_for_3)}
              </span>
              {/* <span className="text-xs font-semibold text-gray-400">PM</span> */}
            </div>
            <div className="flex flex-col items-center">
              <span className="font-semibold text-lg">
                {temp_for_4_index}°C
              </span>
              {is_sunny_for_4 === true ? (
                <img src={sunny} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_coludy_for_4 === true ? (
                <img src={cloudy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              {is_rainy_for_4 === true ? (
                <img src={rainy} className="h-auto w-12"></img>
              ) : (
                ""
              )}
              <span className="font-semibold mt-1 text-sm">
                {convertTo12HourFormat(time_for_4)}
              </span>
              {/* <span className="text-xs font-semibold text-gray-400">PM</span> */}
            </div>
          </div>
        </div>

        {forecastIsAvailable === true && customDate_for_1 !== "" ? (
          <div className="flex flex-col space-y-6 w-full max-w-screen-sm bg-white p-10 mt-10 rounded-xl ring-8 ring-white ring-opacity-40">
            <div className="flex justify-between items-center">
              <span className="font-semibold text-lg w-1/4">
                {customDate_for_1}
              </span>
              <div className="flex items-center justify-end w-1/4 pr-10">
                <span className="font-semibold">{avgHumidity_for_1}%</span>
                <svg
                  className="w-6 h-6 fill-current ml-1"
                  viewBox="0 0 16 20"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g transform="matrix(1,0,0,1,-4,-2)">
                    <path d="M17.66,8L12.71,3.06C12.32,2.67 11.69,2.67 11.3,3.06L6.34,8C4.78,9.56 4,11.64 4,13.64C4,15.64 4.78,17.75 6.34,19.31C7.9,20.87 9.95,21.66 12,21.66C14.05,21.66 16.1,20.87 17.66,19.31C19.22,17.75 20,15.64 20,13.64C20,11.64 19.22,9.56 17.66,8ZM6,14C6.01,12 6.62,10.73 7.76,9.6L12,5.27L16.24,9.65C17.38,10.77 17.99,12 18,14C18.016,17.296 14.96,19.809 12,19.74C9.069,19.672 5.982,17.655 6,14Z" />
                  </g>
                </svg>
              </div>
              {next_1_day_isSunny === true ? (
                <img src={sunny} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              {next_1_day_isColudy === true ? (
                <img src={cloudy} className="h-auto w-10"></img>
              ) : (
                ""
              )}

              {next_1_day_isRainy === true ? (
                <img src={rainy} className="h-auto w-10"></img>
              ) : (
                ""
              )}

              <span className="font-semibold text-lg w-1/4 text-right">
                {minTemp_for_1}° / {maxTemp_for_1}°
              </span>
            </div>
            <div className="flex justify-between items-center">
              <span className="font-semibold text-lg w-1/4">
                {customDate_for_2}
              </span>
              <div className="flex items-center justify-end pr-10 w-1/4">
                <span className="font-semibold">{avgHumidity_for_2}%</span>
                <svg
                  className="w-6 h-6 fill-current ml-1"
                  viewBox="0 0 16 20"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g transform="matrix(1,0,0,1,-4,-2)">
                    <path d="M17.66,8L12.71,3.06C12.32,2.67 11.69,2.67 11.3,3.06L6.34,8C4.78,9.56 4,11.64 4,13.64C4,15.64 4.78,17.75 6.34,19.31C7.9,20.87 9.95,21.66 12,21.66C14.05,21.66 16.1,20.87 17.66,19.31C19.22,17.75 20,15.64 20,13.64C20,11.64 19.22,9.56 17.66,8ZM6,14C6.01,12 6.62,10.73 7.76,9.6L12,5.27L16.24,9.65C17.38,10.77 17.99,12 18,14C18.016,17.296 14.96,19.809 12,19.74C9.069,19.672 5.982,17.655 6,14Z" />
                  </g>
                </svg>
              </div>
              {next_2_day_isSunny === true ? (
                <img src={sunny} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              {next_2_day_isColudy === true ? (
                <img src={cloudy} className="h-auto w-10"></img>
              ) : (
                ""
              )}

              {next_2_day_isRainy === true ? (
                <img src={rainy} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              <span className="font-semibold text-lg w-1/4 text-right">
                {minTemp_for_2}° / {maxTemp_for_2}°
              </span>
            </div>
            <div className="flex justify-between items-center">
              <span className="font-semibold text-lg w-1/4">
                {customDate_for_3}
              </span>
              <div className="flex items-center justify-end pr-10 w-1/4">
                <span className="font-semibold">{avgHumidity_for_3}%</span>
                <svg
                  className="w-6 h-6 fill-current ml-1"
                  viewBox="0 0 16 20"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g transform="matrix(1,0,0,1,-4,-2)">
                    <path d="M17.66,8L12.71,3.06C12.32,2.67 11.69,2.67 11.3,3.06L6.34,8C4.78,9.56 4,11.64 4,13.64C4,15.64 4.78,17.75 6.34,19.31C7.9,20.87 9.95,21.66 12,21.66C14.05,21.66 16.1,20.87 17.66,19.31C19.22,17.75 20,15.64 20,13.64C20,11.64 19.22,9.56 17.66,8ZM6,14C6.01,12 6.62,10.73 7.76,9.6L12,5.27L16.24,9.65C17.38,10.77 17.99,12 18,14C18.016,17.296 14.96,19.809 12,19.74C9.069,19.672 5.982,17.655 6,14Z" />
                  </g>
                </svg>
              </div>
              {next_3_day_isSunny === true ? (
                <img src={sunny} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              {next_3_day_isColudy === true ? (
                <img src={cloudy} className="h-auto w-10"></img>
              ) : (
                ""
              )}

              {next_3_day_isRainy === true ? (
                <img src={rainy} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              <span className="font-semibold text-lg w-1/4 text-right">
                {minTemp_for_3}° / {maxTemp_for_3}°
              </span>
            </div>
            <div className="flex justify-between items-center">
              <span className="font-semibold text-lg w-1/4">
                {customDate_for_4}
              </span>
              <div className="flex items-center justify-end pr-10 w-1/4">
                <span className="font-semibold">{avgHumidity_for_4}%</span>
                <svg
                  className="w-6 h-6 fill-current ml-1"
                  viewBox="0 0 16 20"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g transform="matrix(1,0,0,1,-4,-2)">
                    <path d="M17.66,8L12.71,3.06C12.32,2.67 11.69,2.67 11.3,3.06L6.34,8C4.78,9.56 4,11.64 4,13.64C4,15.64 4.78,17.75 6.34,19.31C7.9,20.87 9.95,21.66 12,21.66C14.05,21.66 16.1,20.87 17.66,19.31C19.22,17.75 20,15.64 20,13.64C20,11.64 19.22,9.56 17.66,8ZM6,14C6.01,12 6.62,10.73 7.76,9.6L12,5.27L16.24,9.65C17.38,10.77 17.99,12 18,14C18.016,17.296 14.96,19.809 12,19.74C9.069,19.672 5.982,17.655 6,14Z" />
                  </g>
                </svg>
              </div>
              {next_4_day_isSunny === true ? (
                <img src={sunny} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              {next_4_day_isColudy === true ? (
                <img src={cloudy} className="h-auto w-10"></img>
              ) : (
                ""
              )}

              {next_4_day_isRainy === true ? (
                <img src={rainy} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              <span className="font-semibold text-lg w-1/4 text-right">
                {minTemp_for_4}° / {maxTemp_for_4}°
              </span>
            </div>
            <div className="flex justify-between items-center">
              <span className="font-semibold text-lg w-1/4">
                {customDate_for_5}
              </span>
              <div className="flex items-center justify-center w-1/4">
                <span className="font-semibold">{avgHumidity_for_5}%</span>
                <svg
                  className="w-6 h-6 fill-current ml-1"
                  viewBox="0 0 16 20"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g transform="matrix(1,0,0,1,-4,-2)">
                    <path d="M17.66,8L12.71,3.06C12.32,2.67 11.69,2.67 11.3,3.06L6.34,8C4.78,9.56 4,11.64 4,13.64C4,15.64 4.78,17.75 6.34,19.31C7.9,20.87 9.95,21.66 12,21.66C14.05,21.66 16.1,20.87 17.66,19.31C19.22,17.75 20,15.64 20,13.64C20,11.64 19.22,9.56 17.66,8ZM6,14C6.01,12 6.62,10.73 7.76,9.6L12,5.27L16.24,9.65C17.38,10.77 17.99,12 18,14C18.016,17.296 14.96,19.809 12,19.74C9.069,19.672 5.982,17.655 6,14Z" />
                  </g>
                </svg>
              </div>
              {next_5_day_isSunny === true ? (
                <img src={sunny} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              {next_5_day_isColudy === true ? (
                <img src={cloudy} className="h-auto w-10"></img>
              ) : (
                ""
              )}

              {next_5_day_isRainy === true ? (
                <img src={rainy} className="h-auto w-10"></img>
              ) : (
                ""
              )}
              <span className="font-semibold text-lg w-1/4 text-right">
                {minTemp_for_5}° / {maxTemp_for_5}°
              </span>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
